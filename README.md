# Paragraphs Access Control

A drupal module to manage access and restriction to edit and view paragraphs.

# Description

This module provides a node access type api to allow restriction of access to 
pragraph items. Companion modules provide access rules, and grants which are
verified when rendering the entities.

# About

This module is developed by the Rutgers University Office of Infomation
Technology. For additional information or support, please contact
webmaster@oit.rutgers.edu.
