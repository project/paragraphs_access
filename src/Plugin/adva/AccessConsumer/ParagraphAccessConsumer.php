<?php

namespace Drupal\paragraphs_access\Plugin\adva\AccessConsumer;

use Drupal\adva\Plugin\adva\OverridingAccessConsumer;

/**
 * @AccessConsumer(
 *  id = "paragraph",
 *  entityType = "paragraph",
 * )
 */
class ParagraphAccessConsumer extends OverridingAccessConsumer {
  
}
